# OpenML dataset: CIFAR_10

https://www.openml.org/d/40927

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Alex Krizhevsky, Vinod Nair, and Geoffrey Hinton    
**Source**: [University of Toronto](https://www.cs.toronto.edu/~kriz/cifar.html) - 2009  
**Please cite**: Alex Krizhevsky (2009) Learning Multiple Layers of Features from Tiny Images, Tech Report.

**CIFAR-10** is a labeled subset of the [80 million tiny images dataset](http://groups.csail.mit.edu/vision/TinyImages/). It (originally) consists 32x32 color images representing 10 classes of objects:  
0. airplane  
1. automobile          
2. bird          
3. cat          
4. deer          
5. dog          
6. frog          
7. horse          
8. ship          
9. truck          

CIFAR-10 contains 6000 images per class. The original train-test split randomly divided these into 5000 train and 1000 test images per class.

The classes are completely mutually exclusive. There is no overlap between automobiles and trucks. "Automobile" includes sedans, SUVs, things of that sort. "Truck" includes only big trucks. Neither includes pickup trucks.

### Attribute description  

Each instance represents a 32x32 colour image as a 3072-value array. The first 1024 entries contain the red channel values, the next 1024 the green, and the final 1024 the blue. The image is stored in row-major order, so that the first 32 entries of the array are the red channel values of the first row of the image.

The labels are encoded as integers in the range 0-9, corresponding to the numbered classes listed above.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40927) of an [OpenML dataset](https://www.openml.org/d/40927). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40927/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40927/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40927/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

